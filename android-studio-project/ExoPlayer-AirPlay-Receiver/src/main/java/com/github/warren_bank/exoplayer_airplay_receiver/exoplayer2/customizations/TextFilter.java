package com.github.warren_bank.exoplayer_airplay_receiver.exoplayer2.customizations;

public interface TextFilter {
  void setTextFilters(String[] textFilters);
  void addTextFilters(String[] textFilters);
}
